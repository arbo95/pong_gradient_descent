import numpy as np
import gym
from gym import wrappers

import tensorflow as tf

env = gym.make("Pong-v0")
# env = gym.make("Breakout-v0")
env = wrappers.Monitor(env, '/tmp/pong-tflog-1')

# hyperparameters
H = 200  # number of hidden layer neurons
batch_size = 10  # every how many episodes to do a param update?
#learning_rate = 1e-4  # Learning rate
start_learning_rate = 1e-3
decrease_frequency = 100
decrease_rate = 1
gamma = 0.99  # discount factor for reward
decay_rate = 0.99  # decay factor for RMSProp leaky sum of grad^2
epsilon = 1e-5
resume = False  # resume from previous checkpoint?
render = False
D = 80 * 80  # input dimensionality: 80x80 grid
logs_path = "/tmp/pong4"
choice_count = env.action_space.n

W1 = tf.get_variable("W1", shape=[D, H], initializer=tf.contrib.layers.xavier_initializer())
W2 = tf.get_variable("W2", shape=[H, choice_count], initializer=tf.contrib.layers.xavier_initializer())
#b1 = tf.get_variable("b1", shape=[H], initializer=tf.contrib.layers.xavier_initializer())
#b2 = tf.get_variable("b2", shape=[2], initializer=tf.contrib.layers.xavier_initializer())

list_of_actions = np.arange(choice_count)

sess = tf.Session()
saver = tf.train.Saver()

if resume:
   saver.restore(sess, "/Users/arnijs/Desktop/Studies/6.sem/linear_regression/gamesave.p")


def prepro(I):
   """ prepro 210x160x3 uint8 frame into 6400 (80x80) 1D float vector """
   I = I[35:195]  # crop
   I = I[::2, ::2, 0]  # downsample by factor of 2
   I[I == 144] = 0  # erase background (background type 1)
   I[I == 109] = 0  # erase background (background type 2)
   I[I != 0] = 1  # everything else (paddles, ball) just set to 1
   return I.astype(np.float).ravel()


observation = env.reset()
prev_x = None  # used in computing the difference frame
xs, advantages, chosenIndex = [], [], []
running_reward = None
episode_number = 0
reward_sum = 0

global_step = tf.Variable(0, trainable=False) # How many times we have trained the model, automatically increased in minimize()
learning_rate = tf.train.exponential_decay(start_learning_rate, global_step, decrease_frequency, decrease_rate, staircase=True)

# forward the policy network and get all action probabilities
xx = tf.placeholder(tf.float32, shape=[None, D], name="xx")
hz = tf.nn.relu(tf.matmul(xx, W1), name="hz")
logp = tf.matmul(hz, W2, name="logp")
p = tf.nn.softmax(logp, name="p")

actionIndices = tf.placeholder(tf.int32, shape=[None], name="actionIndices")
# p = tf.Print(p, [p], 'prr: ', summarize=12)
advantage = tf.placeholder(tf.float32, shape=[None], name="advantages") #Reduce to one dimension [None] and hope to get xx to [None, None]
gathered = tf.gather(tf.reshape(p, [-1]), actionIndices) # Reduce probabilities to 1D and get all the chosen ones in the batch
# gathered = tf.Print(gathered, [gathered], 'gthrd: ', summarize=20)
#advantage = tf.Print(advantage, [advantage], 'advntg: ')
loss = -tf.reduce_sum(tf.multiply(advantage, gathered), name="Loss")
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate, epsilon=epsilon).apply_gradients(loss=loss, global_step=global_step)
with tf.name_scope("summaries"):
    tf.summary.scalar("Loss", loss)
    #tf.summary.scalar("Advantage", advantage)
merged = tf.summary.merge_all()
init = tf.global_variables_initializer()
sess.run(init)
train_writer = tf.summary.FileWriter(logs_path, sess.graph)
frame_num = 0
choices_made = 0

while True:
    if render: env.render()
    frame_num += 1
    # preprocess the observation, set input to network to be difference image
    cur_x = prepro(observation)
    x = cur_x - prev_x if prev_x is not None else np.zeros(D)
    prev_x = cur_x

    probabilities = sess.run(p, {xx: [x]})
    draw = np.random.choice(list_of_actions, 1, p=probabilities[0])
    action = draw[0]

    chosenIndex.append(choices_made * choice_count + action) # Calculate which choice index we took
    choices_made = choices_made + 1

    xs.append(x)  # observation

    # step the environment and get new measurements
    observation, reward, done, info = env.step(action)
    reward_sum += reward
    advantages.append(reward)

    if done:  # an episode finished
        running_sum = 0
        discounted_rewards = np.zeros_like(advantages)
        episode_number += 1
        if episode_number % batch_size == 0:
            for i in reversed(range(len(advantages))):
                if advantages[i] != 0:
                    running_sum = 0
                running_sum = running_sum * gamma + advantages[i]
                discounted_rewards[i] = running_sum
            discounted_rewards -= np.mean(discounted_rewards)
            discounted_rewards /= np.std(discounted_rewards)

            summary = sess.run([merged], feed_dict={advantage: discounted_rewards, xx: xs, actionIndices: chosenIndex})
            for i in range(len(summary)):
                train_writer.add_summary(summary[i], episode_number)

            sess.run(fetches=[optimizer, gathered, actionIndices], feed_dict={advantage: discounted_rewards, xx: xs, actionIndices: chosenIndex})

            xs, advantages, chosenIndex = [], [], []  # reset array memory
            choices_made = 0

        # boring book-keeping
        running_reward = reward_sum if running_reward is None else running_reward * 0.99 + reward_sum * 0.01
        print 'resetting env. episode reward total was %f. running mean: %f' % (reward_sum, running_reward)

        #if episode_number % 100 == 0: saver.save(sess, "/Users/arnijs/Desktop/Studies/6.sem/linear_regression/gamesave.p")

        reward_sum = 0
        observation = env.reset()  # reset env
        prev_x = None

    if reward != 0:  # Pong has either +1 or -1 reward exactly when game ends.
       print ('ep %d: game finished, reward: %f' % (episode_number, reward)) + ('' if reward == -1 else ' !!!!!!!!')